# -*- coding: utf-8 -*-

import smtplib


def create_and_send_email():
    """Funkcja interaktywna pytająca użytkownika o potrzebne dane
    i wysyłająca na ich podstawie list elektroniczny.
    """
    # Odczyt danych od użytkownika
    od = raw_input('Od: ')
    do = raw_input('Do: ')
    temat = raw_input('Temat: ')
    tresc = raw_input('Wiadomosc: ')

    # Stworzenie wiadomości
    email = 'From: ' + od + '\r\n' + 'To: ' + do + '\r\n' + 'Subject: ' + temat + '\r\n\r\n' + tresc

    try:
        # Połączenie z serwerem pocztowym
        s = smtplib.SMTP('194.29.175.240', 25)
        s.set_debuglevel(True)
        s.ehlo()
        # Ustawienie parametrów
        s.starttls()
        s.ehlo()
        # Autentykacja
        s.login('p1', 'p1')
        # Wysłanie wiadomości
        s.sendmail(od, do, email)

    finally:
        # Zamknięcie połączenia
        s.close()


if __name__ == '__main__':
    decision = 't'
    while decision in ['t', 'T', 'y', 'Y']:
        create_and_send_email()
        decision = raw_input(u'Wysłać jeszcze jeden list? ')
