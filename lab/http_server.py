# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config
from email.utils import formatdate


def handle_client(connection, html, logger):
    """Obsługa konwersacji HTTP z pojedynczym klientem

    connection: socket klienta
    html:       wczytana strona html do zwrócenia klientowi
    logger:     mechanizm do logowania wiadomości
    """
    # Odebranie żądania
    # TODO: poprawnie obsłużyć żądanie dowolnego rozmiaru
    request = connection.recv(1024)
    logger.info(u'odebrano: "{0}"'.format(request))

    reqt = request.split('\r\n')

    if reqt[0].find('GET') != -1 and reqt[0].find('HTTP') != -1:
        status = 'HTTP/1.1 200 OK'
    else:
        status = 'HTTP/1.1 405 Method Not Allowed'

    header = status + '\r\n' + 'Content-Type: text/html\r\n' \
             + 'Date: ' + formatdate(timeval=None, localtime=False, usegmt=True) + '\r\n\r\n'

    # Parsowanie URI
    uri = reqt[0].split(' ')[1]
    print 'URI: ' + uri

    # Wysłanie zawartości strony
    connection.sendall(header + html)  # TODO: czy to wszystko?
    logger.info(u'wysyłano odpowiedź')


def http_serve(server_socket, html, logger):
    """Obsługa połączeń HTTP

    server_socket:  socket serwera
    html:           wczytana strona html do zwrócenia klientowi
    logger:         mechanizm do logowania wiadomości
    """
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()
        logger.info(u'połączono z {0}:{1}'.format(*client_address))

        try:
            handle_client(connection, html, logger)

        finally:
            # Zamknięcie połączenia
            connection.close()


def server(logger):
    """Server HTTP

    logger: mechanizm do logowania wiadomości
    """
    # Tworzenie gniazda TCP/IP
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Ustawienie ponownego użycia tego samego gniazda
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Powiązanie gniazda z adresem
    server_address = ('localhost', 44444)  # TODO: zmienić port!
    server_socket.bind(server_address)
    logger.info(u'uruchamiam server na {0}:{1}'.format(*server_address))

    # Nasłuchiwanie przychodzących połączeń
    server_socket.listen(1)

    html = open('web/web_page.html').read()

    try:
        http_serve(server_socket, html, logger)

    finally:
        server_socket.close()


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('http_server')
    server(logger)
    sys.exit(0)