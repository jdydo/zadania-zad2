# -*- encoding: utf-8 -*-

import httplib
import socket


def get_response(host='localhost', port=4444, request_type='', page='/'):
    connection = httplib.HTTPConnection(host, port)

    try:
        connection.request(request_type, page)
        response = connection.getresponse()
    except:
        return '', '', ''
    finally:
        connection.close()

    return response.status, response.reason, response.getheaders()


def get_response_raw(host='localhost', port=4444, header=''):
    connection = socket.socket()
    connection.connect((host, port))

    try:
        connection.sendall(header)
        response = connection.recv(2048)
    except socket.error:
        return ''
    finally:
        connection.close()

    return response


def check_content(headers='', content=''):
    guardian = False
    for header in headers:
        if ('content-type' in header) and (content in header[1]):
            guardian = True
    return guardian