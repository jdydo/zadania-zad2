# -*- encoding: utf-8 -*-

import unittest
from server_methods_for_tests import get_response, get_response_raw, check_content


### Testy wymagają włączonego serwera na komputerze lokalnym lub zdalnym ###
class ServerOnlineTestCase(unittest.TestCase):

    #host = 'localhost'
    host = '194.29.175.240'
    port = 22334

    def test_get_response_and_html_directory_content(self):
        status, reason, headers = get_response(self.host, self.port, 'GET')
        content = check_content(headers, 'text/html')
        self.assertEqual(status, 200)
        self.assertEqual(reason, 'OK')
        self.assertEqual(content, True)

    def test_get_response_and_html_content(self):
        status, reason, headers = get_response(self.host, self.port, 'GET', '/web_page.html')
        content = check_content(headers, 'text/html')
        self.assertEqual(status, 200)
        self.assertEqual(reason, 'OK')
        self.assertEqual(content, True)

    def test_get_response_and_txt_content(self):
        status, reason, headers = get_response(self.host, self.port, 'GET', '/lokomotywa.txt')
        content = check_content(headers, 'text/plain')
        self.assertEqual(status, 200)
        self.assertEqual(reason, 'OK')
        self.assertEqual(content, True)

    def test_get_response_and_jpg_content(self):
        status, reason, headers = get_response(self.host, self.port, 'GET', '/images/jpg_rip.jpg')
        content = check_content(headers, 'image/jpeg')
        self.assertEqual(status, 200)
        self.assertEqual(reason, 'OK')
        self.assertEqual(content, True)

    def test_get_response_and_png_content(self):
        status, reason, headers = get_response(self.host, self.port, 'GET', '/images/gnu_meditate_levitate.png')
        content = check_content(headers, 'image/png')
        self.assertEqual(status, 200)
        self.assertEqual(reason, 'OK')
        self.assertEqual(content, True)

    def test_not_allowed_response(self):
        methods = ['POST', 'PUT', 'DELETE', 'HEAD']
        for method in methods:
            status, reason, headers = get_response(self.host, self.port, method)
            content = check_content(headers, 'text/html')
            self.assertEqual(status, 405)
            self.assertEqual(reason, 'Method Not Allowed')
            self.assertEqual(content, True)

    def test_404_directory(self):
        status, reason, headers = get_response(self.host, self.port, 'GET', '/folder/')
        content = check_content(headers, 'text/html')
        self.assertEqual(status, 404)
        self.assertEqual(reason, 'Not Found')
        self.assertEqual(content, True)

    def test_404_content(self):
        status, reason, headers = get_response(self.host, self.port, 'GET', '/obrazek.jpg')
        content = check_content(headers, 'text/html')
        self.assertEqual(status, 404)
        self.assertEqual(reason, 'Not Found')
        self.assertEqual(content, True)

    def test_headers(self):
        status, reason, headers = get_response(self.host, self.port, 'GET', '/obrazek.jpg')

        i = 0
        for header in headers:
            if 'date' in header[0] or 'content-type' in header[0] or 'content-length' in header[0] and header[1]:
                i += 1

        self.assertEqual(i, 3)
        self.assertEqual(len(headers), 3)

    def test_headers_raw_sip_protocol_request(self):
        request = 'INVITE sip:bob@biloxi.com SIP/2.0' + '\r\n' \
                    + 'Via: SIP/2.0/UDP pc33.atlanta.com;branch=z9hG4bK776asdhds' + '\r\n' \
                    + 'Max-Forwards: 70' + '\r\n' \
                    + 'To: Bob <sip:bob@biloxi.com>' + '\r\n' \
                    + 'From: Alice <sip:alice@atlanta.com>;tag=1928301774' + '\r\n' \
                    + 'Call-ID: a84b4c76e66710@pc33.atlanta.com' + '\r\n' \
                    + 'CSeq: 314159 INVITE' + '\r\n' \
                    + 'Contact: <sip:alice@pc33.atlanta.com>' + '\r\n' \
                    + 'Content-Type: application/sdp' + '\r\n' \
                    + 'Content-Length: 142' + '\r\n\r\n'
        response = get_response_raw(self.host, self.port, request)
        main_header = response.split('\r\n')[0]
        self.assertEqual(main_header, 'HTTP/1.1 405 Method Not Allowed')

    def test_headers_raw_odd_request(self):
        client_headers = 'ążśęćó łńasd_ 123[]];.../ w \r\nążśęćó łńasd_ 123[]];.../ w \r\nążśęćó3[]];.../ w \r\n\r\n'
        server_headers = get_response_raw(self.host, self.port, client_headers)
        main_header = server_headers.split('\r\n')[0]
        self.assertEqual(main_header, 'HTTP/1.1 405 Method Not Allowed')


if __name__ == '__main__':
    unittest.main()
